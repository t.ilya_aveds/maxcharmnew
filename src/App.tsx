import * as React from "react";
import SecondSec from "./components/secondSec/secondSec";
import ThirdSec from "./components/thirdSec/thirdSec";
import FourthSec from "./components/fourthSec/fourthSec";
import FifthSec from "./components/fifthSec/fifthSec";
import SixthSec from "./components/sixthSec/sixthSec";
import SeventhSec from "./components/seventhSec/seventhSec";
import EighthSec from "./components/eighthSec/eighthSec";
import Main from "./components/main/main";
import NinthSec from "./components/ninthSec/ninthSec";
import Footer from "./components/footer/footer";

function App() {
    return (
        <>
            <Main />
            <SecondSec />
            <ThirdSec />
            <FourthSec />
            <FifthSec />
            <SixthSec />
            <SeventhSec />
            <EighthSec />
            <NinthSec />
            <Footer />
        </>
    );
}

export default App;
