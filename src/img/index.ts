import bgImg from "./main-bg.jpg";
import logoImg from "./logo.svg";
import playImg from "./play.svg";
import arrowLink from "./arrowLink.svg";
import cloud1 from "./cloud1.svg";
import cloud2 from "./cloud2.svg";
import cloud3 from "./cloud3.svg";
import cloud4 from "./cloud4.svg";
import giftImg from "./gift.svg";
import drinksImg from "./drinks.svg";
import foodImg from "./food.svg";
import materialImg from "./meterial.svg";
import face1 from "./face1.png";
import face2 from "./face2.png";
import judge1 from "./judge1.jpg";
import judge2 from "./judge2.jpg";
import judge3 from "./judge3.jpg";
import member1 from "./member1.png";
import member2 from "./member2.png";
import member3 from "./member3.png";
import masterClassImg from "./masterClassImg.png";
import finallyImg from "./finallyImg.png";
import logoDark from "./logoDark.svg";
import star from "./star.svg";
import programmImg1 from "./programm1.jpg";
import programmImg2 from "./programm2.jpg";
import programmImg3 from "./programm3.jpg";
import programmImg4 from "./programm4.jpg";
import ligthStar from "./lightStar.svg";
import bgMobile from "./mobileBG.jpg";

export {
    bgMobile,
    ligthStar,
    star,
    logoDark,
    finallyImg,
    masterClassImg,
    member1,
    member2,
    member3,
    judge1,
    judge2,
    judge3,
    bgImg,
    logoImg,
    playImg,
    arrowLink,
    face1,
    face2,
    cloud1,
    cloud2,
    cloud3,
    cloud4,
    giftImg,
    foodImg,
    materialImg,
    drinksImg,
    programmImg1,
    programmImg2,
    programmImg3,
    programmImg4,
};
