import "./secondSec.scss";
import * as React from "react";
import Question from "./question";
import ImgCircle from "./imgCircle";
import { cloud1, cloud2, cloud3, cloud4 } from "../../img";

const SecondSec = () => {
    return (
        <section className="secondSec section">
            <div className="mainImgWrapper">
                <ImgCircle />
                <ul className="questionsList">
                    <Question
                        pos="1"
                        img={cloud1}
                        name="Думаешь ехать или нет?"
                    />
                    <Question
                        pos="2"
                        img={cloud2}
                        name="Найти место и время, чтобы что-то перекусить…"
                    />
                    <Question
                        pos="3"
                        img={cloud3}
                        name="Несколько чемоданов на хрупких плечах с материалами для
                        выступления…"
                    />
                    <Question
                        pos="4"
                        img={cloud4}
                        name="Нужно решить, где остановиться в Москве на пару дней…"
                    />
                </ul>
            </div>
            <p className="text">
                ЗАБУДЬ ОБ ЭТОМ!!!
                <span className="spanText">
                    В ОРГВЗНОС ВКЛЮЧЕНО ВСЁ – «ALL INCLUSIVE»
                </span>
            </p>
        </section>
    );
};

export default SecondSec;
