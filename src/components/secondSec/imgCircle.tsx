import "./secondSec.scss";
import * as React from "react";
import { face1 } from "../../img";

const ImgCircle = () => {
    return (
        <div className="imgCircle">
            <img className="mainImg" src={face1} alt="face" />
        </div>
    );
};

export default ImgCircle;
