import * as React from "react";
import "./secondSec.scss";

interface IQuestion {
    img: string;
    name: string;
    pos: string;
}

const Question: React.FC<IQuestion> = ({ img, name, pos }) => {
    return (
        <li className={`questionWrapper${pos}`}>
            <div className={"question"}>
                <img src={img} alt="" />
                <p>{name}</p>
            </div>
        </li>
    );
};

export default Question;
