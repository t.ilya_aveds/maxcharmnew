import "swiper/scss";
import "swiper/css/pagination";
import { Swiper } from "swiper/react";
import { SwiperSlide } from "swiper/react";
import { Navigation, Pagination, Scrollbar, A11y } from "swiper/modules";
import * as React from "react";

const MasterClasses = ({ list }) => {
    return (
        <>
            <ul className="mcList">
                {list.map((item) => (
                    <div className="mcItem" key={item.id}>
                        <img src={item.img} alt="" />
                        <h2>{item.title}</h2>
                        <p>{item.desc}</p>
                        <div>
                            <h2 className="price">{item.price}₽</h2>
                            {/* eslint-disable */}
                            <a className="link" href="#">
                                Купить
                            </a>
                            {/* eslint-enable */}
                        </div>
                    </div>
                ))}
            </ul>

            <div className="mcSlider">
                <Swiper
                    spaceBetween={20}
                    pagination={{ clickable: true }}
                    modules={[Navigation, Pagination, Scrollbar, A11y]}
                    slidesPerView={"auto"}
                    breakpoints={{
                        960: {
                            slidesPerView: "4",
                        },
                        760: {
                            slidesPerView: "3",
                        },
                        450: {
                            slidesPerView: "2",
                        },
                    }}
                >
                    {list.map((item) => (
                        <SwiperSlide key={item.id}>
                            <div className="mcItem" key={item.id}>
                                <img src={item.img} alt="" />
                                <h2>{item.title}</h2>
                                <p>{item.desc}</p>
                                <div>
                                    <h2 className="price">{item.price}₽</h2>
                                    {/* eslint-disable */}
                                    <a className="link" href="#">
                                        Купить
                                    </a>
                                    {/* eslint-enable */}
                                </div>
                            </div>
                        </SwiperSlide>
                    ))}
                </Swiper>
            </div>
        </>
    );
};

export default MasterClasses;
