import "./eighthSec.scss";
import * as React from "react";
import SecHeader from "../../common/secHeader";
import MasterClasses from "./masterClasses";
import { masterClassImg } from "../../img";
const EighthSec = () => {
    const listOfMC = [
        {
            id: 1,
            img: masterClassImg,
            title: "Мастер-класс №1",
            desc: `Чат с кураторами
Проверка домашних заданий
2 дня эфиров
Записи эфиров с доступом 
на 7 дней`,
            price: 590,
        },
        {
            id: 2,
            img: masterClassImg,
            title: "Мастер-класс №1",
            desc: `Чат с кураторами
Проверка домашних заданий
2 дня эфиров
Записи эфиров с доступом 
на 7 дней`,
            price: 590,
        },
        {
            id: 3,
            img: masterClassImg,
            title: "Мастер-класс №1",
            desc: `Чат с кураторами
Проверка домашних заданий
2 дня эфиров
Записи эфиров с доступом 
на 7 дней`,
            price: 590,
        },
        {
            id: 4,
            img: masterClassImg,
            title: "Мастер-класс №1",
            desc: `Чат с кураторами
Проверка домашних заданий
2 дня эфиров
Записи эфиров с доступом 
на 7 дней`,
            price: 590,
        },
        {
            id: 5,
            img: masterClassImg,
            title: "Мастер-класс №1",
            desc: `Чат с кураторами
Проверка домашних заданий
2 дня эфиров
Записи эфиров с доступом 
на 7 дней`,
            price: 590,
        },
        {
            id: 6,
            img: masterClassImg,
            title: "Мастер-класс №1",
            desc: `Чат с кураторами
Проверка домашних заданий
2 дня эфиров
Записи эфиров с доступом 
на 7 дней`,
            price: 590,
        },
        {
            id: 7,
            img: masterClassImg,
            title: "Мастер-класс №1",
            desc: `Чат с кураторами
Проверка домашних заданий
2 дня эфиров
Записи эфиров с доступом 
на 7 дней`,
            price: 590,
        },
        {
            id: 8,
            img: masterClassImg,
            title: "Мастер-класс №1",
            desc: `Чат с кураторами
Проверка домашних заданий
2 дня эфиров
Записи эфиров с доступом 
на 7 дней`,
            price: 590,
        },
        {
            id: 9,
            img: masterClassImg,
            title: "Мастер-класс №1",
            desc: `Чат с кураторами
Проверка домашних заданий
2 дня эфиров
Записи эфиров с доступом 
на 7 дней`,
            price: 590,
        },
        {
            id: 10,
            img: masterClassImg,
            title: "Мастер-класс №1",
            desc: `Чат с кураторами
Проверка домашних заданий
2 дня эфиров
Записи эфиров с доступом 
на 7 дней`,
            price: 590,
        },
    ];

    return (
        <section className="eighthSec section">
            <SecHeader
                linkTo="#"
                title="Мастер-классы"
                linkName="Смотреть все"
            />
            <MasterClasses list={listOfMC} />
        </section>
    );
};

export default EighthSec;
