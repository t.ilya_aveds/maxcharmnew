import "swiper/scss";
import "swiper/css/pagination";
import { Swiper } from "swiper/react";
import { SwiperSlide } from "swiper/react";
import { Navigation, Pagination, Scrollbar, A11y } from "swiper/modules";
import * as React from "react";
import "./seventhSec.scss";
import SecHeader from "../../common/secHeader";
import { member1, member2, member3 } from "../../img";

const SeventhSec = () => {
    const members = [
        {
            img: member1,
            name: "Анастасия",
            info: "Мастер биодепиляции г. Москва",
        },
        {
            img: member2,
            name: "Анна",
            info: "Мастер биодепиляции г. Москва",
        },
        {
            img: member3,
            name: "Виктория",
            info: "Мастер депиляции г. Санкт-Петербург",
        },
        {
            img: member1,
            name: "Анастасия1",
            info: "Мастер биодепиляции г. Москва",
        },
        {
            img: member2,
            name: "Анна1",
            info: "Мастер биодепиляции г. Москва",
        },
        {
            img: member3,
            name: "Виктория1",
            info: "Мастер депиляции г. Санкт-Петербург",
        },
        {
            img: member1,
            name: "Анастасия2",
            info: "Мастер биодепиляции г. Москва",
        },
        {
            img: member2,
            name: "Анна2",
            info: "Мастер биодепиляции г. Москва",
        },
        {
            img: member3,
            name: "Виктория2",
            info: "Мастер депиляции г. Санкт-Петербург",
        },
    ];

    return (
        <section className="seventhSec sectionTwo">
            <div className="section">
                <SecHeader
                    title="Участники"
                    linkName="Смотреть все"
                    linkTo="#"
                />
                <Swiper
                    spaceBetween={20}
                    pagination={{ clickable: true }}
                    modules={[Navigation, Pagination, Scrollbar, A11y]}
                    slidesPerView={"3"}
                    breakpoints={{
                        1200: {
                            slidesPerView: "3",
                        },
                        620: {
                            slidesPerView: "2",
                        },
                        0: {
                            slidesPerView: "1",
                        },
                    }}
                >
                    <div className="judgesSlider">
                        {members.map((item) => (
                            <SwiperSlide key={item.name}>
                                <div className="item" key={item.id}>
                                    <img src={item.img} alt="" />
                                    <h2 className="itemName">{item.name}</h2>
                                    <p className="itemInfo">{item.info}</p>
                                </div>
                            </SwiperSlide>
                        ))}
                    </div>
                </Swiper>
            </div>
        </section>
    );
};

export default SeventhSec;
