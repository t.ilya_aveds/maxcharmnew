import "./nineSec.scss";
import * as React from "react";
import { finallyImg } from "../../img";

const NinthSec = () => {
    return (
        <section className="sectionTwo nineSec">
            {/* eslint-disable */}
            <div className="section">
                <div className="banner">
                    <div className="info">
                        <p className="address">Москва, 10 -12 ноября 2023 г</p>
                        <p className="address">ГОСТИНИЦА ИЗМАЙЛОВО «БЕТА»</p>
                        <h2 className="title">
                            Регистрируйтесь на <span>I</span> международный
                            чемпионат "MaxCharm-2023"
                        </h2>
                        <p className="text">
                            Не пропустите главное событие осени
                            <br />
                            Подайте заявку и участвуйте в чемпионате{" "}
                        </p>
                        <a className="btn" href="#">
                            Подать заявку
                        </a>
                    </div>
                    <img src={finallyImg} alt="" />
                </div>
            </div>
            {/* eslint-enable */}
        </section>
    );
};

export default NinthSec;
