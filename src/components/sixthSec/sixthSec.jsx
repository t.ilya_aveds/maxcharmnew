import "swiper/scss";
import "swiper/css/pagination";
import { Swiper } from "swiper/react";
import { SwiperSlide } from "swiper/react";
import { Navigation, Pagination, Scrollbar, A11y } from "swiper/modules";
import "./sixthSec.scss";
import { judge1, judge2, judge3 } from "../../img";
import SecHeader from "../../common/secHeader";
import * as React from "react";

const SixthSec = () => {
    const judgesList = [
        {
            img: judge1,
            name: "Юлия Мазурок",
            achievements: [
                "Основатель ТМ «Sugar Life» выпускающий широкую линейку профессионального продукта - сахарной пасты для депиляции и полную серию пред и постэпиляционного ухода за кожей",
                "Основатель и руководитель тренинг - студии «Sugar Life», одного из сильнейших учебных центров в Казахстане, подтвержденный сертификатом Ассоциации индустрии красоты",
                "Профессиональный преподаватель",
                "Сертифицированный мастер - технолог, со стажем 13 лет.",
                "Судья чемпионатов по эпиляции",
                "Владелица салона красоты Sugar Lifе",
            ],
        },
        {
            img: judge2,
            name: "Наталья Рябинова",
            achievements: [
                "Легендарный методист, стоявший у истоков развития профессиональной эпиляции.",
                "Руководитель сети салонов и учебного центра Epil Center RIABINOVA",
                "Международный судья, спикер, организатор чемпионатов и конференций",
                "Производитель косметики для шугаринга и ухода за кожей",
                "Писатель, автор первого в мире объёмного иллюстрированного учебника по шугарингу",
                "Основатель онлайн-академии RIABINOVA ACADEMY, где проводится несколько топовых курсов по шугарингу и бьюти-бизнесу.",
            ],
            info: "Нет предела совершенству! Участие в чемпионате - это не только яркое событие, но и возможность познакомиться с интересными людьми, узнать много нового, наполниться энергией успеха!",
        },
        {
            img: judge3,
            name: "Пронина Анна",
            achievements: [
                "Преподаватель по шугарингу и восковой депиляции",
                "Основатель и руководитель школы депиляции IRIS г. Красноярск",
                "Основатель школы депиляции и салона красоты в г. Абакане",
                "Создатель программ и семинаров по развитию мастеров депиляции",
                'Призер полуфинала III Международного Чемпионата по эпиляции в г. Красноярске в номинации "Эпиляция. Профессионал" 2015 г.',
                "Победитель состязания Sugaring Battle в г. Красноярске 2016 г.",
                "Владелец первого и крупнейшего интернет-магазина для мастеров депиляции в Красноярском крае",
                "Организатор полуфинала V AsiaEpilOpen 2019 в г. Красноярске",
            ],
            info: "Профессионалами не рождаются, ими становятся и мы точно знаем как!",
        },
        {
            img: judge1,
            name: "Юлия Мазурок2",
            achievements: [
                "Основатель ТМ «Sugar Life» выпускающий широкую линейку профессионального продукта - сахарной пасты для депиляции и полную серию пред и постэпиляционного ухода за кожей",
                "Основатель и руководитель тренинг - студии «Sugar Life», одного из сильнейших учебных центров в Казахстане, подтвержденный сертификатом Ассоциации индустрии красоты",
                "Профессиональный преподаватель",
                "Сертифицированный мастер - технолог, со стажем 13 лет.",
                "Судья чемпионатов по эпиляции",
                "Владелица салона красоты Sugar Lifе",
            ],
        },
        {
            img: judge2,
            name: "Наталья Рябинова2",
            achievements: [
                "Легендарный методист, стоявший у истоков развития профессиональной эпиляции.",
                "Руководитель сети салонов и учебного центра Epil Center RIABINOVA",
                "Международный судья, спикер, организатор чемпионатов и конференций",
                "Производитель косметики для шугаринга и ухода за кожей",
                "Писатель, автор первого в мире объёмного иллюстрированного учебника по шугарингу",
                "Основатель онлайн-академии RIABINOVA ACADEMY, где проводится несколько топовых курсов по шугарингу и бьюти-бизнесу.",
            ],
            info: "Нет предела совершенству! Участие в чемпионате - это не только яркое событие, но и возможность познакомиться с интересными людьми, узнать много нового, наполниться энергией успеха!",
        },
        {
            img: judge3,
            name: "Пронина Анна2",
            achievements: [
                "Преподаватель по шугарингу и восковой депиляции",
                "Основатель и руководитель школы депиляции IRIS г. Красноярск",
                "Основатель школы депиляции и салона красоты в г. Абакане",
                "Создатель программ и семинаров по развитию мастеров депиляции",
                'Призер полуфинала III Международного Чемпионата по эпиляции в г. Красноярске в номинации "Эпиляция. Профессионал" 2015 г.',
                "Победитель состязания Sugaring Battle в г. Красноярске 2016 г.",
                "Владелец первого и крупнейшего интернет-магазина для мастеров депиляции в Красноярском крае",
                "Организатор полуфинала V AsiaEpilOpen 2019 в г. Красноярске",
            ],
            info: "Профессионалами не рождаются, ими становятся и мы точно знаем как!",
        },
        {
            img: judge1,
            name: "Юлия Мазурок3",
            achievements: [
                "Основатель ТМ «Sugar Life» выпускающий широкую линейку профессионального продукта - сахарной пасты для депиляции и полную серию пред и постэпиляционного ухода за кожей",
                "Основатель и руководитель тренинг - студии «Sugar Life», одного из сильнейших учебных центров в Казахстане, подтвержденный сертификатом Ассоциации индустрии красоты",
                "Профессиональный преподаватель",
                "Сертифицированный мастер - технолог, со стажем 13 лет.",
                "Судья чемпионатов по эпиляции",
                "Владелица салона красоты Sugar Lifе",
            ],
        },
        {
            img: judge2,
            name: "Наталья Рябинова3",
            achievements: [
                "Легендарный методист, стоявший у истоков развития профессиональной эпиляции.",
                "Руководитель сети салонов и учебного центра Epil Center RIABINOVA",
                "Международный судья, спикер, организатор чемпионатов и конференций",
                "Производитель косметики для шугаринга и ухода за кожей",
                "Писатель, автор первого в мире объёмного иллюстрированного учебника по шугарингу",
                "Основатель онлайн-академии RIABINOVA ACADEMY, где проводится несколько топовых курсов по шугарингу и бьюти-бизнесу.",
            ],
            info: "Нет предела совершенству! Участие в чемпионате - это не только яркое событие, но и возможность познакомиться с интересными людьми, узнать много нового, наполниться энергией успеха!",
        },
        {
            img: judge3,
            name: "Пронина Анна3",
            achievements: [
                "Преподаватель по шугарингу и восковой депиляции",
                "Основатель и руководитель школы депиляции IRIS г. Красноярск",
                "Основатель школы депиляции и салона красоты в г. Абакане",
                "Создатель программ и семинаров по развитию мастеров депиляции",
                'Призер полуфинала III Международного Чемпионата по эпиляции в г. Красноярске в номинации "Эпиляция. Профессионал" 2015 г.',
                "Победитель состязания Sugaring Battle в г. Красноярске 2016 г.",
                "Владелец первого и крупнейшего интернет-магазина для мастеров депиляции в Красноярском крае",
                "Организатор полуфинала V AsiaEpilOpen 2019 в г. Красноярске",
            ],
            info: "Профессионалами не рождаются, ими становятся и мы точно знаем как!",
        },
    ];

    return (
        <section className="sixthSec section">
            <SecHeader
                title="Судейский состав"
                linkName="Смотреть все"
                linkTo="#"
            />
            <Swiper
                spaceBetween={20}
                pagination={{ clickable: true }}
                modules={[Navigation, Pagination, Scrollbar, A11y]}
                slidesPerView={"3"}
                breakpoints={{
                    1200: {
                        slidesPerView: "3",
                    },
                    620: {
                        slidesPerView: "2",
                    },
                    0: {
                        slidesPerView: "1",
                    },
                }}
            >
                <div className="judgesSlider">
                    {judgesList.map((item) => (
                        <SwiperSlide key={item.name}>
                            <div className="item" key={item.id}>
                                <img src={item.img} alt="" />
                                <h2 className="itemName">{item.name}</h2>
                                {item.achievements?.length && (
                                    <ul className="achievementsList">
                                        {item.achievements.map((achive) => (
                                            <li key={achive}>{achive}</li>
                                        ))}
                                    </ul>
                                )}
                                {item.info && (
                                    <p className="itemInfo">{item.info}</p>
                                )}
                            </div>
                        </SwiperSlide>
                    ))}
                </div>
            </Swiper>
        </section>
    );
};

export default SixthSec;
