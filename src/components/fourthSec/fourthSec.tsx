import "./fourthSec.scss";
import * as React from "react";
import {
    programmImg1,
    programmImg2,
    programmImg3,
    programmImg4,
} from "../../img";
import ProgrammItem from "./programmItem";
import SecHeader from "../../common/secHeader";

const FourthSec = () => {
    return (
        <section className="fourthSec section">
            <SecHeader
                linkTo="#"
                title="Программа чемпионата"
                linkName="Скачать программу"
                note={true}
            />
            <div className="programmWrapper">
                <ul className="programmList">
                    <ProgrammItem
                        img={programmImg1}
                        number="01"
                        time="10 ноября 10.00-17.00"
                        title="Мастер-классы и программы повышения квалификации."
                    />
                    <ProgrammItem
                        img={programmImg2}
                        number="02"
                        time="10 ноября 18.00.-20.00"
                        title="Торжественная церемония открытия Чемпионата. Фуршет."
                    />
                    <ProgrammItem
                        img={programmImg3}
                        number="03"
                        time="11 ноября 09.00-20.00"
                        title="Конкурсная часть чемпионата."
                    />
                    <ProgrammItem
                        img={programmImg4}
                        number="04"
                        time="12 ноября 12.00-16.00"
                        title="Торжественная церемония награждения победителей Чемпионата. Праздничный банкет."
                    />
                </ul>
            </div>
        </section>
    );
};

export default FourthSec;
