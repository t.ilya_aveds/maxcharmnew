import * as React from "react";
import { star } from "../../img";

interface IProgrammItem {
    img: string;
    time: string;
    title: string;
    number: string;
}

const ProgrammItem: React.FC<IProgrammItem> = ({
    time,
    title,
    number,
    img,
}) => {
    return (
        <li className="programmItem">
            <div className="imgWrapper">
                <img src={img} alt="" />
            </div>
            <div className="vl">
                <img src={star} alt="" />{" "}
            </div>
            <div className="programmInfoWrapper">
                <div className="programmInfo">
                    <p className="programmNumb">{number}</p>
                    <p className="programmTime">{time}</p>
                    <p className="programmTitle">{title}</p>
                </div>
            </div>
        </li>
    );
};

export default ProgrammItem;
