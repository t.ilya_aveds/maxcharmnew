import "./footer.scss";
import * as React from "react";
import { logoDark } from "../../img";

const Footer = () => {
    return (
        <div className="section footer">
            <img src={logoDark} alt="" />
            <a className="phoneNumb" href="tel:+89999999999">
                8 (999) 999-99-99
            </a>
        </div>
    );
};

export default Footer;
