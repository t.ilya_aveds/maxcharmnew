import "./thirdSec.scss";
import * as React from "react";

interface IServiceItem {
    img: string;
    title: string;
    desc: string;
}

const ServiceItem: React.FC<IServiceItem> = ({ img, title, desc }) => {
    return (
        <li className="serviceItem">
            <div className="imgWrapper">
                <img src={img} alt="" />
            </div>
            <h4>{title}</h4>
            <p>{desc}</p>
        </li>
    );
};

export default ServiceItem;
