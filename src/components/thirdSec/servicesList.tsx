import * as React from "react";
import ServiceItem from "./serviceItem";

interface IServiceItem {
    img: string;
    title: string;
    desc: string;
}
interface IServiceList {
    list: IServiceItem[];
}
const ServicesList: React.FC<IServiceList> = ({ list }) => {
    return (
        <ul className="servicesList">
            {list.map((item) => (
                <ServiceItem
                    key={item.title}
                    img={item.img}
                    title={item.title}
                    desc={item.desc}
                />
            ))}
        </ul>
    );
};

export default ServicesList;
