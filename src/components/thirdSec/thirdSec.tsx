import * as React from "react";
import "./thirdSec.scss";
import ServicesList from "./servicesList";
import { drinksImg, foodImg, giftImg, materialImg } from "../../img";

const ThirdSec = () => {
    const servicesList = [
        {
            img: foodImg,
            title: "Проживание и питание",
            desc: "MaxCharm гарантирует вам проживание вместе с питанием",
        },
        {
            img: giftImg,
            title: "ВСЕ материалы для выступления",
            desc: "Мы предоставим лучшие материалы для выполнения работ",
        },
        {
            img: materialImg,
            title: "Огромное количество подарков от спонсоров",
            desc: "Вы получите невероятное количество замечательных подарков от спонсоров",
        },
        {
            img: drinksImg,
            title: "Фуршет на открытии и банкет на закрытии Чемпионата",
            desc: "На нашем фуршете вы сможете найти новые знакомства и просто хорошо провести время",
        },
    ];

    return (
        <section className="thirdSec sectionTwo">
            <div className="section">
                <ServicesList list={servicesList} />
            </div>
        </section>
    );
};

export default ThirdSec;
