import "./fifthSec.scss";
import * as React from "react";
import { face2 } from "../../img";
import Pros from "./pros";
import SecHeader from "../../common/secHeader";

const FifthSec = () => {
    return (
        <section className="fifthSec sectionTwo">
            <div className="section">
                <div className="advantage">
                    <SecHeader title="Что даст тебе чемпионат?" />
                    <div className="imgWrapper">
                        <img src={face2} alt="" />
                        <Pros
                            name="Новые знакомства с талантливыми участниками"
                            number="04"
                        />
                        <Pros
                            name="Взаимодействие с известными судьями"
                            number="03"
                        />
                        <Pros name="Привлечение новых клиентов" number="01" />
                        <Pros name="Повышение квалификации" number="02" />
                    </div>
                </div>
            </div>
        </section>
    );
};

export default FifthSec;
