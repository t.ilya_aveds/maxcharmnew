import * as React from "react";
import { ligthStar } from "../../img";

interface IPros {
    name: string;
    number: string;
}
const Pros: React.FC<IPros> = ({ name, number }) => {
    return (
        <div className={`prosWrapper prosWrapper${+number}`}>
            <img className="star" src={ligthStar} alt="" />
            <p className="prosTitle">{name}</p>
        </div>
    );
};

export default Pros;
