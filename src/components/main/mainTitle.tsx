import * as React from "react";

const MainTitle = () => {
    return (
        <h1 className="title">
            <span className="numbSpan">I</span> международный чемпионат по
            биодепиляции <span className="noWrap">"MaxCharm-2023"</span>
        </h1>
    );
};

export default MainTitle;
