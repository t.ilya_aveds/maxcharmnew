import * as React from "react";

interface IMainBg {
    img: string;
}

const MainBg: React.FC<IMainBg> = ({ img }) => {
    return (
        <div className="bgContainer">
            <div className="bgWrapper">
                <img className="bgImg" src={img} alt="background-img" />
                <div className="bgColor"></div>
            </div>
        </div>
    );
};

export default MainBg;
