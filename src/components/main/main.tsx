import "./main.scss";
import MainBg from "./mainBg";
import * as React from "react";
import MainInfo from "./mainInfo";
import MainHeader from "./mainHeader";
import { bgImg, logoImg } from "../../img";

const Main = () => {
    return (
        <main className="main">
            <div className="section">
                <MainBg img={bgImg} />
                <MainHeader img={logoImg} />
                <MainInfo />
            </div>
        </main>
    );
};

export default Main;
