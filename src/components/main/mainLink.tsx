import * as React from "react";

interface IMainLink {
    linkName: string;
    linkTo: string;
}

const MainLink: React.FC<IMainLink> = ({ linkName, linkTo }) => {
    return (
        <a href={linkTo} className="linkMoreWrapper">
            <p className="linkMore">{linkName}</p>
            <svg
                width="13"
                height="13"
                viewBox="0 0 13 13"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
            >
                <g clipPath="url(#clip0_0_354)">
                    <path
                        d="M10.0387 1.82186L0.675781 11.1847L1.54377 12.0527L10.9066 2.68985L11.0827 8.76988L12.2653 8.42483L12.0846 2.18452C12.0608 1.35782 11.3696 0.666682 10.5429 0.64275L4.46497 0.466797L4.11885 1.64837L10.0387 1.82186Z"
                        fill="white"
                        stroke="white"
                        strokeWidth="0.542084"
                    />
                </g>
                <defs>
                    <clipPath id="clip0_0_354">
                        <rect width="13" height="13" fill="white" />
                    </clipPath>
                </defs>
            </svg>
        </a>
    );
};

export default MainLink;
