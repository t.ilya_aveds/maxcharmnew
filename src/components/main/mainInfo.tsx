import * as React from "react";
import MainBtns from "./mainBtns";
import MainLink from "./mainLink";
import MainTitle from "./mainTitle";
import MainAddress from "./mainAddress";

const MainInfo = () => {
    return (
        <div className="mainInfo">
            <MainAddress />
            <MainTitle />
            <MainBtns />
            <MainLink linkName="Положение о чемпионате" linkTo="#" />
        </div>
    );
};

export default MainInfo;
