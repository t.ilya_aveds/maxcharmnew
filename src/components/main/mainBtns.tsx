import * as React from "react";
import { playImg } from "../../img";

const MainBtns = () => {
    return (
        <ul className="btns">
            {/* eslint-disable */}
            <a href={"#"} className="btn btnLigth">
                Подать заявку
            </a>
            <a href={"#"} className="btn btnDark">
                <img className="btnImg" src={playImg} />
                Посмотреть видео
            </a>
            {/* eslint-enable */}
        </ul>
    );
};

export default MainBtns;
