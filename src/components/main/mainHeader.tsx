import * as React from "react";

interface IMainHeader {
    img: string;
}

const MainHeader: React.FC<IMainHeader> = ({ img }) => {
    return (
        <header className="mainHeader header">
            <img src={img} alt="logo-img" />
            <a href="tel:+89999999999">8 (999) 999-99-99</a>
        </header>
    );
};

export default MainHeader;
