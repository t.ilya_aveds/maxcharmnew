import * as React from "react";

const MainAddress = () => {
    return (
        <address>
            <ul className="addressList">
                <li className="address">Москва, 10 -12 ноября 2023 г</li>
                <li className="address">ГОСТИНИЦА ИЗМАЙЛОВО «БЕТА»</li>
            </ul>
        </address>
    );
};

export default MainAddress;
