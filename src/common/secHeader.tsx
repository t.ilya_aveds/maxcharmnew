import * as React from "react";
import "./common.scss";
import { star } from "../img";

interface ISecHeader {
    title: string;
    linkName?: string;
    linkTo?: string;
    note?: boolean;
}

const SecHeader: React.FC<ISecHeader> = ({ title, linkName, linkTo, note }) => {
    return (
        <div className={"secHeader" + (linkName ? "" : " secHeaderCenter")}>
            <div>
                <h2 className={note ? "h2Note" : ""}>{title}</h2>
                <img className="star" src={star} alt="" />
            </div>
            {linkName && (
                <a className="link" href={linkTo}>
                    {linkName}
                </a>
            )}
        </div>
    );
};

export default SecHeader;
